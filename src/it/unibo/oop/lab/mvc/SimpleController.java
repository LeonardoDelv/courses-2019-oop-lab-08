package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class SimpleController implements Controller {
    
    private final List<String> historyStrings = new LinkedList<>();
    private String nextString;

    @Override
    public void setNextStringToPrint(String nextString) {
        // TODO Auto-generated method stub
        this.nextString = Objects.requireNonNull(nextString, "This method requires a non-null input");
    }

    @Override
    public String getStringToPrint() {
        // TODO Auto-generated method stub
        return this.nextString;
    }

    @Override
    public List<String> getPrintedStrings() {
        // TODO Auto-generated method stub
        return this.historyStrings;
    }

    @Override
    public void printCurrentString() {
        // TODO Auto-generated method stub
        if (this.nextString == null) {
            throw new IllegalStateException("There is no string set");
        }
        historyStrings.add(this.nextString);
        System.out.println(this.nextString);
    }

}
